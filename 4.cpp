﻿#include "pch.h"
#include <iostream>
#include <math.h>
using namespace std;

int main()
{
	double a;
	double b = 3;
	double c = 0.2;
	double x;
	for (a = 1; a <= b; a = a + c) {
		x = pow(a, 3) - 3 * pow(a, 2);
		cout << "f1(x)= " << x << endl;
	}
	a = 1;
	while (a < b) {
		a = a + c;
		x = pow(a, 4) - 2 * pow(a, 2) + 3;
		cout << "f2(x)= " << x << endl;

	}
	return 0;
}