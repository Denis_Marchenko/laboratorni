﻿#include "pch.h"
#include <iostream>
#include <math.h>

using namespace std;

float H(float a, float b) {
	static float h;
	h = 4 * pow(a, 2) + 5 * pow(b, 2);
	return h;
}
float A(int & x) {
	static float a;
	a = cos(x) + 2 * x;
	return a;
}
float B(int * x) {
	static float b;
	b = 4 * *x - (2 * *x);
	return b;
}
float C(int * x) {
	static float c;
	c = 2 * *x - 5;
	return c;


}



int main()
{
	double a, b, c, h;
	int x;
	cout << "Enter the number x: ";
	cin >> x;
	cout << "Your number A: " << A(x) << endl;
	a = A(x);
	cout << "Your number B: " << B(&x) << endl;
	b = B(&x);
	cout << "Your number C: " << C(&x) << endl;
	c = C(&x);
	cout << "Your number H: " << H(a, b) << endl;

	return 0;

}