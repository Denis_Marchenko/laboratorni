﻿import datetime

def difference(x: int, y : int) -> int:
    """
    Calculating the difference between entered date and current date

    :param (int|Current date) x: Current date
    :param (int|Entered date) y: Entered date
    :raises ValueError if year, month or day have entered incorrectly
    :return (int): The difference between entered date and current date
    """
    diff = y - x
    return diff
    
a = int(input('Enter a year:'))
b = int(input('Enter a month:'))
c = int(input('Enter a day:'))

current_time = datetime.date.today()
gained_time = datetime.date(a,b,c)
diff = gained_time - current_time

difference(current_time, gained_time)

if diff.days <= 5 and diff.days >= -5:
    print('The difference between your date and current day is on', diff.days, 'days')
else:
    print('The difference between your date and current day is too big')
