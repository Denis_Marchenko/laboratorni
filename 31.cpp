﻿#include "pch.h"
#include <iostream>
#include <cmath>
#define _USE_MATH_DEFINES
#define M_E        2.71828182845904523536

using namespace std;

int main()
{
	setlocale(LC_ALL, "Russian");

	double x;
	double z;
	double y;

	cout << "Введите значение переменной z" << endl;
	cin >> z;

	if (z < 0)
		x = pow(z, 3) - 3 * (pow(z, 2));

	else if (z >= 0 && z <= 8)
		x = sin(z);

	else
		x = pow(M_E, 2) - pow(M_E, -z);

	y = pow(x, 2) + 8 * x - 6;
	cout << y;

	return 0;
}

