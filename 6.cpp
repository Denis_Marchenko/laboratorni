﻿#include "pch.h"
#include <iostream>
#include <cstdlib>
#include <string.h>
#include <math.h>

using namespace std;

int main()
{
	setlocale(LC_ALL, "Russian");
	int n = 4;
	const int MAX = 500;
	int i;
	double mx = 0;


	struct people {
		char fip[MAX];
		int bros;
		int day;
		int month;
		int year;
		double weight;
	};

	people hum[5];
	strcpy_s(hum[0].fip, "Романенко Глеб Викторович");
	strcpy_s(hum[1].fip, "Ульянова Полина Сергеевна");
	strcpy_s(hum[2].fip, "Мальцев Алексей Денисович");
	strcpy_s(hum[3].fip, "Алабаев Тимур Борисович");
	strcpy_s(hum[4].fip, "Черноус Варвара Николаевна");

	for (i = 0; i <= n; i++)
		hum[i].bros = rand() % 10;

	for (i = 0; i <= n; i++)
		hum[i].day = rand() % 10;

	for (i = 0; i <= n; i++)
		hum[i].month = 1990 + rand() % 12 + 9;

	for (i = 0; i <= n; i++)
		hum[i].year = rand() % 2;

	for (i = 0; i <= n; i++)
		hum[i].weight = (rand() % 101 + 550) / 10.0;

	for (i = 0; i <= n; i++)
		if (mx <= hum[i].weight)
			mx = hum[i].weight;

	cout << mx << endl;

	return 0;
}
